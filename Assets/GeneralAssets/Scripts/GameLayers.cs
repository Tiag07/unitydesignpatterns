﻿using UnityEngine;

public class GameLayers : MonoBehaviour
{
    public static int PlayerLayer { get; private set; } = 10;
    public static int CoinLayer { get; private set; } = 8;
}
