﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformMoveController : MonoBehaviour
{
    [SerializeField] private string horizontalAxis = "Horizontal";
    [SerializeField] private string verticalAxis = "Vertical";
    [SerializeField] private float moveSpeed = 2f;

    private void Update()
    {
        MoveTransform();
    }

    void MoveTransform()
    {
        float horizontalValue = Input.GetAxisRaw(horizontalAxis);
        float verticalValue = Input.GetAxisRaw(verticalAxis);
        Vector3 newMovement = new Vector3(horizontalValue, 0, verticalValue);
        transform.position += newMovement * moveSpeed * Time.deltaTime;
    }
}
