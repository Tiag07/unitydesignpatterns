﻿using UnityEngine;

public class LoopRotation : MonoBehaviour
{
    [SerializeField] private Vector3 rotateFactor;
    void Update()
    {
        transform.Rotate(rotateFactor * Time.deltaTime);
    }
}
