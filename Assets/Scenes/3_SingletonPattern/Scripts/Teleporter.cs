﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class Teleporter : MonoBehaviour
    {
        [SerializeField] private Object sceneToTeleport;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == GameLayers.PlayerLayer)
            {
                GameManager.Instance.LoadScene(sceneToTeleport.name);
            }

        }
    }
}