﻿using UnityEngine;
using UnityEngine.UI;

namespace SingletonPattern
{
    public class UI_ScoreManager_Singleton : MonoBehaviour
    {
        [SerializeField] Text txtCurrentCoins;
        [SerializeField] string coinsLabelText = "Coins: {0}";
        private void OnEnable()
        {
            GameManager.Instance.coinScoreRefreshed += RefreshCoinScoreText;
        }
        private void OnDisable()
        {
            GameManager.Instance.coinScoreRefreshed -= RefreshCoinScoreText;
        }

        void RefreshCoinScoreText(int currentCoins)
        {
            txtCurrentCoins.text = string.Format(coinsLabelText, currentCoins.ToString());
        }
    }
}