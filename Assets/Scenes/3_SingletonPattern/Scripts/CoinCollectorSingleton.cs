﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class CoinCollectorSingleton : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == GameLayers.CoinLayer)
            {
                other.gameObject.SetActive(false);
                GameManager.Instance.AddCoin();
            }
        }
    }
}