﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

namespace SingletonPattern
{
    public class GameManager : PermanentSingleton<GameManager>
    {
        [SerializeField] private int _currentCoins = 0;
        public Action<int> coinScoreRefreshed;

        protected override void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            base.OnSceneLoaded(scene, mode);
            RefreshCoinScore();
        }

        public void AddCoin()
        {
            _currentCoins += 1;
            RefreshCoinScore();
        }
        private void RefreshCoinScore()
        {
            coinScoreRefreshed?.Invoke(_currentCoins);
        }

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}