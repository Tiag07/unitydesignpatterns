﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SingletonPattern
{
    public class Singleton<SomeComponent> : MonoBehaviour where SomeComponent : Component
    {
        private static SomeComponent _instance;

        public static SomeComponent Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject newObj = new GameObject();
                    newObj.name = typeof(SomeComponent).Name;
                    _instance = newObj.AddComponent<SomeComponent>();
                }
                return _instance;
            }
        }

        private void OnDestroy()
        {
            if (_instance == this)
            {
                _instance = null;
            }
        }
    }
    public class PermanentSingleton<SomeComponent> : MonoBehaviour where SomeComponent : Component
    {
        private static SomeComponent _instance;
        public static SomeComponent Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject newObj = new GameObject();
                    newObj.name = typeof(SomeComponent).Name;
                    _instance = newObj.AddComponent<SomeComponent>();

                    DontDestroyOnLoad(_instance);                  
                }
                return _instance;
            }
        }

        private void OnDestroy()
        {
            if (_instance == this)
            {
                _instance = null;
            }
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        protected virtual void OnSceneLoaded(Scene scene, LoadSceneMode mode) { }
    }

}