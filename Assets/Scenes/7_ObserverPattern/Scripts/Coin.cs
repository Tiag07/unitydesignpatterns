﻿using UnityEngine;
using System;

namespace ObserverPattern
{
    public class Coin : MonoBehaviour
    {
        [SerializeField] private int coinValue = 1;

        static public event Action<int> collected; 

        public void OnCollected()
        {
            collected?.Invoke(coinValue);
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.layer == GameLayers.PlayerLayer)
            {
                OnCollected();
            }
        }
    }
}