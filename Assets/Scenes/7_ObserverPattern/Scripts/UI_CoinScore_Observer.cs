﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ObserverPattern
{
    public class UI_CoinScore_Observer : MonoBehaviour
    {
        private int _coinCount = 0;

        [SerializeField] private Text txtCoinScore;
        [SerializeField] private string coinBaseText;

        private void Awake()
        {
            Coin.collected += RefreshCoinScore;
        }

        private void Start()
        {
            RefreshCoinScore(_coinCount);
        }

        private void RefreshCoinScore(int coinsToAdd)
        {
            _coinCount += coinsToAdd;
            txtCoinScore.text = string.Format(coinBaseText, _coinCount);
        }
    }
}