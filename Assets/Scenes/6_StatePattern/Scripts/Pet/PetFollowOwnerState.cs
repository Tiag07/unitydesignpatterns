﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern.Pet
{
    public class PetFollowOwnerState : IPetState
    {
        private Transform _petOwner;
        float distanceToStop = 2f;
        public PetFollowOwnerState(Transform owner)
        {
            _petOwner = owner;
        }
        public void BeginPhase(Pet pet)
        {
            Debug.Log("Follow Owner Starts!");
            
        }

        public void UpdatePhase(Pet pet)
        {
            Debug.Log("Following Owner");
            pet.moveManager.Move((_petOwner.position));

            if (pet.moveManager.OnDestination(distanceToStop)) 
                pet.SwitchState(pet.IdleState);
        }
    }
}