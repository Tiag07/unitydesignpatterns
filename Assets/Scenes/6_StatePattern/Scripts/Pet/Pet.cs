﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern.Pet
{
    public class Pet : MonoBehaviour
    {
        [SerializeField] private Transform petOwner;
        [SerializeField] private float _moveSpeed = 3f;

        [SerializeField] private Transform[] waypoints;

        public IPetState currentState;
        public PetPatrolState PatrolState { get; private set; }
        public PetFollowOwnerState FollowOwnerState { get; private set; }
        public PetIdleState IdleState { get; private set; }
        public MoveToDestination moveManager { get; private set; }

        private void Start()
        {
            moveManager = new MoveToDestination(transform, _moveSpeed);
            PatrolState = new PetPatrolState(waypoints);
            FollowOwnerState = new PetFollowOwnerState(petOwner);
            IdleState = new PetIdleState(petOwner);

            currentState = PatrolState;
            currentState.BeginPhase(this);
        }
        private void Update()
        {
            currentState.UpdatePhase(this);
        }

        public void SwitchState(IPetState newState)
        {
            currentState = newState;
            currentState.BeginPhase(this);
        }
        
        public void Button_Follow() => SwitchState(FollowOwnerState);
        public void Button_Patrol() => SwitchState(PatrolState);

    }
}