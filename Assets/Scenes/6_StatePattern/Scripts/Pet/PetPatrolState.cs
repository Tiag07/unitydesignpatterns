﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern.Pet
{
    public class PetPatrolState : IPetState
    {
        private int _currentWaypointId;
        private Transform[] _waypoints;
      
        public PetPatrolState(Transform[] waypoints, int startWaypointId = 0)
        {
            _waypoints = waypoints;
            _currentWaypointId = startWaypointId;
        }
        public void BeginPhase(Pet pet)
        {
            Debug.Log("PatrolStart!");
        }

        public void UpdatePhase(Pet pet)
        {
            Debug.Log("Patrolling");
            pet.moveManager.Move(_waypoints[_currentWaypointId].position);

            if (pet.moveManager.OnDestination()) GoToNextWayPoint();

            void GoToNextWayPoint()
            {
                _currentWaypointId++;
                if(_currentWaypointId >= _waypoints.Length)
                    _currentWaypointId = 0;
            }            
        }

    }
}