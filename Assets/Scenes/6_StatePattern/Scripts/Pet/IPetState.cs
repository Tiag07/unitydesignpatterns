﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern.Pet
{
    public interface IPetState
    {
        void BeginPhase(Pet stateManager);
        void UpdatePhase(Pet stateManager);
    }
}