﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern
{
    public class MoveToDestination
    {
        private Transform _walker;
        private Vector3 _destination;
        private float _walkSpeed = 3f;
        public MoveToDestination(Transform walker, float walkSpeed = 3f)
        {
            _walker = walker;
            _walkSpeed = walkSpeed;
        }
        public void Move(Vector3 newDestination)
        {
            _destination = new Vector3(newDestination.x, _walker.position.y, newDestination.z);
            _walker.LookAt(_destination);
            _walker.position += _walker.forward * _walkSpeed * Time.deltaTime;
        }
        public bool OnDestination(float distanceToStop = 0.1f) => Vector3.Distance(_walker.position, _destination) < distanceToStop;
    }
}