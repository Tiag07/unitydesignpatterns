﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern.Pet
{
    public class PetIdleState : IPetState
    {
        private float _distanceToFollow = 3f;
        private Transform _petOwner;
        public PetIdleState(Transform owner)
        {
            _petOwner = owner;
        }
        public void BeginPhase(Pet pet)
        {
            Debug.Log("Stopped!");
        }

        public void UpdatePhase(Pet pet)
        {
            Debug.Log("Waiting owner's movement!");
            if (Vector3.Distance(pet.transform.position, _petOwner.position) > _distanceToFollow)
            {
                pet.SwitchState(pet.FollowOwnerState);
            }
        }
    }
}