﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DecoratorPattern.UI
{
    public class UI_DummyInfo : MonoBehaviour
    {
        [SerializeField] private StandardDamageableObject dummy;
        [SerializeField] private Text txtDummyHp;

        private void Awake()
        {
            dummy.hpChanged += RefreshDummyHp;
        }
        void RefreshDummyHp(float currentHp)
        {
            txtDummyHp.text = currentHp.ToString();
        }
    }
}