﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern.UI
{
    public class UI_ShootConfig : MonoBehaviour
    {
        public string CannonBallDamage
        {
            set
            {
                GameDatabase.Damagers.CannonBall_Damage = float.Parse(value);
            }
        }

    }
}