﻿using DecoratorPattern.VFXLibrary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class DamagerDecorator : MonoBehaviour
    {
        [SerializeField] private Gun gun;
        [SerializeField] private PowerUpVFX_Library vfxLibrary;
        [SerializeField] private bool _flamePowerUp;
        [SerializeField] private bool _poisonPowerUp;
        [SerializeField] private bool _electricPowerUp;

        public bool FlamePowerUp { get => _flamePowerUp; set => _flamePowerUp = value; }
        public bool PoisonPowerUp { get => _poisonPowerUp; set => _poisonPowerUp = value; }
        public bool ElectricPowerUp { get => _electricPowerUp; set => _electricPowerUp = value; }

        private void Start()
        {
            gun.bulletInstantiated += Decorate;
            gun.bulletInstantiated += AddVisualEffect;
        }

        void Decorate(DamagerObject damager)
        {
            if (_flamePowerUp) damager.AddNewPowerUp(new FlamePowerUp(damager));
            if (_poisonPowerUp) damager.AddNewPowerUp(new PoisonPowerUp(damager));
            if (_electricPowerUp) damager.AddNewPowerUp(new ElectricPowerUp(damager));
        }
        void AddVisualEffect(DamagerObject damager)
        {
            if (vfxLibrary == null) return;
            if (_flamePowerUp)
            {
                Instantiate(vfxLibrary.FlameEffect, damager.transform);
            }

            if (_poisonPowerUp)
            {
                GameObject poisonOrb = Instantiate(vfxLibrary.PoisonEffect, damager.transform);
                poisonOrb.transform.localScale *= 1.1f;
            }

            if (_electricPowerUp)
            {
                Instantiate(vfxLibrary.SparksEffect, damager.transform);
            }
        }
    }
}