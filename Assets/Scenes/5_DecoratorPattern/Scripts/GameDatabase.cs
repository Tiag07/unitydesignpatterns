﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class GameDatabase : MonoBehaviour
    {
        public class Damagers
        {
            //It is not "readonly" just for allow editing in scene example
            public static float CannonBall_Damage = 20; 
        }
        public class PowerUps
        {
            public static readonly float Flame_Damage = 10;

            public static readonly float Poison_DamagePerSecond = 5;
            public static readonly float Poison_DurationInSeconds = 4;

            public static readonly float Electric_Damage = 5;
            public static readonly float Electric_Paralize_DurationInSeconds = 3;

        }
    }
}