﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern.VFXLibrary
{
    [CreateAssetMenu(fileName = "PowerUpVFX_Library", menuName = "ScriptableObjects/DecoratorPattern/PowerUpVFX_Library")]
    public class PowerUpVFX_Library : ScriptableObject
    {
        [SerializeField] private ParticleSystem flameEffect;
        [SerializeField] private GameObject poisonEffect;
        [SerializeField] private ParticleSystem sparksEffect;

        public ParticleSystem FlameEffect { get => flameEffect; }

        public GameObject PoisonEffect { get => poisonEffect; }
        public ParticleSystem SparksEffect { get => sparksEffect; }
    }
}