﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern.VFXLibrary
{
    [CreateAssetMenu(fileName = "StatusVFX_Library", menuName = "ScriptableObjects/DecoratorPattern/StatusVFX_Library")]

    public class StatusVFX_Library : ScriptableObject
    {
        [SerializeField] private ParticleSystem poisonedStatusEffect;
        [SerializeField] private ParticleSystem paralizedStatusEffect;

        public ParticleSystem PoisonedStatusEffect { get => poisonedStatusEffect; }
        public ParticleSystem ParalizedStatusEffect { get => paralizedStatusEffect; }
    }
}