﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public interface IDamager
    {
        void OnContact(IDamageable damageable);
        void ImproveDamage(float bonusDamage);
    }
}