﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class CannonBall : DamagerObject
    {
        [SerializeField] private float force = 10;
        [SerializeField] private Rigidbody rb;
        void Start()
        {
            rb.AddForce(transform.forward * force, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            IDamageable target = other.gameObject.GetComponent<IDamageable>();
            if (target != null)
            {
                baseDamage = GameDatabase.Damagers.CannonBall_Damage;
                base.OnContact(target);
                gameObject.SetActive(false);
            }
        }
    }
}