﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class DamagerObject : MonoBehaviour, IDamager
    {
        protected float baseDamage = 0;
        private List<DamagePowerUp> powerUps = new List<DamagePowerUp>();

        public void ImproveDamage(float bonusDamage)
        {
            baseDamage += bonusDamage;
        }

        public void OnContact(IDamageable damageable)
        {
            foreach(var powerUp in powerUps)
            {
                powerUp.OnContact(damageable);
            }
            damageable.ReceiveDamage(baseDamage);
        }

        public void AddNewPowerUp(DamagePowerUp newPowerUp)
        {
            powerUps.Add(newPowerUp);
        }
    }
}