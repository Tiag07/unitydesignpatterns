﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class FlamePowerUp : DamagePowerUp
    {
        public FlamePowerUp(IDamager damagerToImprove) : base(damagerToImprove) { }
        protected override void ApplyEffect()
        {
            float flameDamage = GameDatabase.PowerUps.Flame_Damage;
            _damagerToImprove.ImproveDamage(flameDamage);
        }
    }
}