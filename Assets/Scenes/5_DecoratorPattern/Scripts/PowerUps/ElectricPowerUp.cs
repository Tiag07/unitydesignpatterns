﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class ElectricPowerUp : DamagePowerUp
    {
        public ElectricPowerUp(IDamager damagerToImprove) : base(damagerToImprove) { }
        protected override void ApplyEffect()
        {
            float electricDamage = GameDatabase.PowerUps.Electric_Damage;
            float paralizeDurationInSeconds = GameDatabase.PowerUps.Electric_Paralize_DurationInSeconds;
            _damagerToImprove.ImproveDamage(electricDamage);
            _target.ReceiveParalizeStaus(paralizeDurationInSeconds);
        }
    }
}