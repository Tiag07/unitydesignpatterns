﻿namespace DecoratorPattern
{
    public abstract class DamagePowerUp : IDamager
    {
        protected IDamager _damagerToImprove;
        protected IDamageable _target;
        protected abstract void ApplyEffect();
        public DamagePowerUp(IDamager damagerToImprove)
        {
            _damagerToImprove = damagerToImprove;
        }
        public void ImproveDamage(float bonusDamage)
        {
        }

        public void OnContact(IDamageable damageable)
        {
            _target = damageable;
            ApplyEffect();
        }
    }
}