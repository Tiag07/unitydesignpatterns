﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    public class PoisonPowerUp : DamagePowerUp
    {
        public PoisonPowerUp(IDamager damagerToImprove) : base(damagerToImprove) { }
        protected override void ApplyEffect()
        {
            float dmgPerSec = GameDatabase.PowerUps.Poison_DamagePerSecond;
            float poisonDuration = GameDatabase.PowerUps.Poison_DurationInSeconds;
            _target.ReceivePoisonStatus(dmgPerSec, poisonDuration);
         }
    }
}