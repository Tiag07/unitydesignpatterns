﻿namespace DecoratorPattern
{
    public interface IDamageable
    {
        void ReceiveDamage(float damage);
        void ReceivePoisonStatus(float damagePerSecond, float durationInSeconds);
        void ReceiveParalizeStaus(float durationInSeconds);
    }
}