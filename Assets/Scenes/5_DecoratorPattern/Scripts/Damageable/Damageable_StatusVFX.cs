﻿using DecoratorPattern.VFXLibrary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecoratorPattern
{
    [RequireComponent(typeof(StandardDamageableObject))]
    public class Damageable_StatusVFX : MonoBehaviour
    {
        [SerializeField] private StatusVFX_Library statusVfxLibrary;
        [SerializeField] private float effectSize = 1;

        private StandardDamageableObject _damageable;
        private ParticleSystem poisonedEffect;
        private ParticleSystem paralizedEffect;
        private void Awake()
        {
            _damageable = GetComponent<StandardDamageableObject>();
            _damageable.poisonStatusChanged += OnPoisonedStatusChanged;
            _damageable.paralizeStatusChanged += OnParalizedStatusChanged;
        }
        void OnPoisonedStatusChanged(bool state)
        {
            if (poisonedEffect == null)
            {
                poisonedEffect = Instantiate(statusVfxLibrary.PoisonedStatusEffect, transform);
                poisonedEffect.transform.localScale = Vector3.one * effectSize;
            }

            poisonedEffect.gameObject.SetActive(state);
        }
        void OnParalizedStatusChanged(bool state)
        {
            if (paralizedEffect == null)
            {
                paralizedEffect = Instantiate(statusVfxLibrary.ParalizedStatusEffect, transform);
                paralizedEffect.transform.localScale = Vector3.one * effectSize;
            }
            paralizedEffect.gameObject.SetActive(state);

        }
    }
}