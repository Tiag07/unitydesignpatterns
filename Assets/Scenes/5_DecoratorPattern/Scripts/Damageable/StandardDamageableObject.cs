﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace DecoratorPattern
{
    public class StandardDamageableObject : MonoBehaviour, IDamageable
    {
        [SerializeField] private float maxHp = 100;
        private float currentHp = 100;
        [SerializeField] private bool invencible = false;

        private bool isPoisoned = false;
        private bool isParalized = false;

        //For UI changes and status particles
        public Action<float> hpChanged;
        public Action<bool> poisonStatusChanged;
        public Action<bool> paralizeStatusChanged;

        private Coroutine paralizeCoroutine;


        private void Start()
        {
            currentHp = maxHp;
            hpChanged?.Invoke(currentHp);
        }
        public void ReceiveDamage(float damage)
        {
            currentHp -= damage;

            if (currentHp <= 0)
            {
                if (invencible)
                    currentHp = maxHp;
                else
                    currentHp = 0;
            }
            hpChanged?.Invoke(currentHp);
        }

        public virtual void ReceivePoisonStatus(float damage, float durationInSeconds)
        {
            //That effect is not stackable.
            if (isPoisoned) return;

            StartCoroutine(DamageCicleCoroutine());
            IEnumerator DamageCicleCoroutine()
            {
                isPoisoned = true;
                poisonStatusChanged?.Invoke(true);
                for (int i = 0; i < durationInSeconds; i++)
                {
                    yield return new WaitForSeconds(1);
                    ReceiveDamage(damage);
                }
                isPoisoned = false;
                poisonStatusChanged?.Invoke(false);
            }
        }

        public virtual void ReceiveParalizeStaus(float durationInSeconds)
        {
            //That effect can be overwited, but paralize duration is not stackable.
            if (isParalized) StopCoroutine(paralizeCoroutine);

            paralizeCoroutine = StartCoroutine(ParalizeCoroutine());
            IEnumerator ParalizeCoroutine()
            {
                isParalized = true;
                paralizeStatusChanged?.Invoke(true);

                yield return new WaitForSeconds(durationInSeconds);

                isParalized = false;
                paralizeStatusChanged?.Invoke(false);
            }
        }
    }
}