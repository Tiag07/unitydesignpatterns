﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace DecoratorPattern
{
    public class Gun : MonoBehaviour
    {
        [SerializeField] private DamagerObject bulletPrefab;
        [SerializeField] private Transform bulletSpawnPoint;
        public Action<DamagerObject> bulletInstantiated;
        public void Button_Shoot()
        {
            DamagerObject newBullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            bulletInstantiated?.Invoke(newBullet);
        }
    }
}