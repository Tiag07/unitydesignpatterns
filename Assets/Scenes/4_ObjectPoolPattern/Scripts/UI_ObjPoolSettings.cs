﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ObjectPoolPattern
{
    public class UI_ObjPoolSettings : MonoBehaviour
    {       
        [SerializeField] private InputField inputField_CubeLifeTime;
        [SerializeField] private ObjectPool cubeObjectPool;

        [SerializeField] private Button btnDoNothing;
        [SerializeField] private Button btnCreateNew;
        [SerializeField] private Button btnGetOlder;

        [SerializeField] private Text txtObjCount;
        [SerializeField] private Text txtInactiveObjectCount;
        public void Input_OnValueChanged_SetCubeLifeTime()
        {
            float timeInSeconds = float.Parse(inputField_CubeLifeTime.text);
            if (timeInSeconds == 0)
                timeInSeconds = 0.1f;

            DisableAfterSeconds.SetLifeTime(timeInSeconds);
        }
        private void Start()
        {
            btnDoNothing.onClick.AddListener(Button_DoNothing);
            btnCreateNew.onClick.AddListener(Button_CreateNew);
            btnGetOlder.onClick.AddListener(Button_GetOlder);

            Button_DoNothing();
        }
        private void Update()
        {
            txtObjCount.text = cubeObjectPool.GetTotalObjectCount().ToString();
            txtInactiveObjectCount.text = cubeObjectPool.GetInactiveObjectCount().ToString();
        }
        void ResetButtons()
        {
            btnDoNothing.interactable = true;
            btnCreateNew.interactable = true;
            btnGetOlder.interactable = true;
        }
        public void Button_DoNothing()
        {
            ResetButtons();
            cubeObjectPool.SetWhenNoInactiveObjectseMode_DoNothing();
            btnDoNothing.interactable = false;
        }
        public void Button_CreateNew()
        {
            ResetButtons();
            cubeObjectPool.SetWhenNoInactiveObjectseMode_CreateNewObject();
            btnCreateNew.interactable = false;
        }
        public void Button_GetOlder()
        {
            ResetButtons();
            cubeObjectPool.SetWhenNoInactiveObjectsMode_GetOlderObject();
            btnGetOlder.interactable = false;
        }
    }
}