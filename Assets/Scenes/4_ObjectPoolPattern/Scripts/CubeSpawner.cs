﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPoolPattern
{
    public class CubeSpawner : MonoBehaviour
    {
        [SerializeField]
        private ObjectPool objectPool;

        [SerializeField]
        private Transform[] spawnPoints;

        [SerializeField]
        private float spawnIntervalInSeconds = 0.5f;

        private void Start()
        {
            StartCoroutine(SpawnLoopCoroutine());
        }
        private IEnumerator SpawnLoopCoroutine()
        {
            yield return new WaitForSeconds(spawnIntervalInSeconds);
            if (objectPool == null) yield return null;

            GameObject newObject = objectPool.GetObject();
            if(newObject != null)
            {
                newObject.transform.position = GetRandomSpawnPoint().position;
                newObject.transform.rotation = GetRandomRotation();                
            }

            StartCoroutine(SpawnLoopCoroutine());
        }

        private Transform GetRandomSpawnPoint()
        {
            if (spawnPoints.Length == 1) return spawnPoints[0];

            return spawnPoints[Random.Range(0, spawnPoints.Length)];
        }
        private Quaternion GetRandomRotation()
        {
            float randomX = Random.Range(0, 360);
            float randomY = Random.Range(0, 360);
            float randomZ = Random.Range(0, 360);

            return Quaternion.Euler(new Vector3(randomX, randomY, randomZ));
        }

    }
}