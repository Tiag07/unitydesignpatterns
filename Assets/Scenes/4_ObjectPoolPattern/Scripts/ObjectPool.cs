﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPoolPattern
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField]
        private GameObject objPrefab;

        [SerializeField]
        private int startMaxAmount = 10;

        [SerializeField]
        private Queue<GameObject> objQueue;

        public enum WhenNoInactiveObjects { CreateNewObject, GetOlderObject, DoNothing }
        [SerializeField] 
        private WhenNoInactiveObjects whenNoInactiveObjects;

        private void Start()
        {
            objQueue = new Queue<GameObject>(startMaxAmount);
            for (int i = 0; i < startMaxAmount; i++)
            {
                CreateNewObject(false);
            }
        }

        private GameObject CreateNewObject(bool active = false)
        {
            GameObject newObj = Instantiate(objPrefab);
            newObj.SetActive(active);
            newObj.transform.parent = transform;
            objQueue.Enqueue(newObj);
            return newObj;
        }

        public GameObject GetObject()
        {
            foreach (var obj in objQueue)
            {
                if (obj.activeInHierarchy == false)
                {
                    obj.SetActive(true);
                    return obj;
                }
            }

            switch (whenNoInactiveObjects)
            {
                case WhenNoInactiveObjects.CreateNewObject:
                    return CreateNewObject(true);                   

                case WhenNoInactiveObjects.GetOlderObject:
                    return GetOlderObject();                   

                case WhenNoInactiveObjects.DoNothing:
                    break;
            }
            return null;
        }
        private GameObject GetOlderObject()
        {
            GameObject olderObj = objQueue.Peek();
            objQueue.Dequeue();
            objQueue.Enqueue(olderObj);
            return olderObj;
        }

        #region SETTINGS AREA
        public int GetTotalObjectCount() => objQueue.Count;
        public int GetInactiveObjectCount()
        {
            int objAmount = 0;
            foreach(var obj in objQueue)
            {
                if (obj.activeInHierarchy == false) 
                    objAmount++;
            }
            return objAmount;
        }
            
        public void SetWhenNoInactiveObjectseMode_DoNothing()
        {
            whenNoInactiveObjects = WhenNoInactiveObjects.DoNothing;
        }
        public void SetWhenNoInactiveObjectseMode_CreateNewObject()
        {
            whenNoInactiveObjects = WhenNoInactiveObjects.CreateNewObject;
        }
        public void SetWhenNoInactiveObjectsMode_GetOlderObject()
        {
            whenNoInactiveObjects = WhenNoInactiveObjects.GetOlderObject;
        }
        #endregion
    }
}