﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPoolPattern
{
    public class DisableAfterSeconds : MonoBehaviour
    {
        public static float s_lifeTime = 4f;
        private float timeAmount;
        private void OnEnable()
        {
            ResetTimer();
        }
        private void Update()
        {
            if (timeAmount < s_lifeTime)
            {
                timeAmount += Time.deltaTime;
            }
            else if (gameObject.activeInHierarchy)
            {
                gameObject.SetActive(false);
            }
        }
        public void ResetTimer() => timeAmount = 0;
        public static void SetLifeTime(float newLifetime) => s_lifeTime = newLifetime;
    }
}