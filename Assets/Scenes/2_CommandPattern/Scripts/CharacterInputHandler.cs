﻿using UnityEngine;
using System;

namespace CommandPattern
{
    public class CharacterInputHandler : MonoBehaviour
    {
        [SerializeField] private Keymap keymap;
        [SerializeField] private MoveSettings moveSettings;       
        [SerializeField] private CharacterMovement charMovement;
        private CommandManager commandManager;
        public Action<string> commandActivated;
        public Action undoCommandActivated;
        private void Start()
        {
            commandManager = new CommandManager();
        }

        private void Update()
        {
            CheckInputs();
        }

        void CheckInputs()
        {
            if (charMovement.InMovingProcess) return;
            if (keymap.InChangingProcess) return;

            if (Input.GetKeyDown(keymap.KeyUp))
            {
                ICommand upCommand = new MoveCommand(charMovement, moveSettings.upMovement);
                commandManager.AddAndExecuteCommand(upCommand);
                commandActivated?.Invoke("Move Up");
            }
            else if (Input.GetKeyDown(keymap.KeyDown))
            {
                ICommand downCommand = new MoveCommand(charMovement, moveSettings.downMovement);
                commandManager.AddAndExecuteCommand(downCommand);
                commandActivated?.Invoke("Move Down");
            }
            else if (Input.GetKeyDown(keymap.KeyLeft))
            {
                ICommand leftCommand = new MoveCommand(charMovement, moveSettings.leftMovement);
                commandManager.AddAndExecuteCommand(leftCommand);
                commandActivated?.Invoke("Move Left");
            }
            else if (Input.GetKeyDown(keymap.KeyRight))
            {
                ICommand rightCommand = new MoveCommand(charMovement, moveSettings.rightMovement);
                commandManager.AddAndExecuteCommand(rightCommand);
                commandActivated?.Invoke("Move Right");
            }
            else if (Input.GetKeyDown(keymap.KeyUndo))
            {
                commandManager.UndoLastCommand();
                undoCommandActivated?.Invoke();
            }
        }
    }
}