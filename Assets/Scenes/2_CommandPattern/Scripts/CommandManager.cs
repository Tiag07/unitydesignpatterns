﻿using System.Collections.Generic;

namespace CommandPattern
{
    public class CommandManager
    {
        private Stack<ICommand> _commandsStacked;
        public CommandManager()
        {
            _commandsStacked = new Stack<ICommand>();
        }

        public void AddAndExecuteCommand(ICommand newCommand)
        {
            newCommand.Execute();
            _commandsStacked.Push(newCommand);
        }

        public void UndoLastCommand()
        {
            if (_commandsStacked.Count == 0) return;

            var lastCommand = _commandsStacked.Pop();
            lastCommand.Undo();
        }

    }
}