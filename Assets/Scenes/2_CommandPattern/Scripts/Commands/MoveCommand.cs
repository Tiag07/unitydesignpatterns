﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandPattern
{
    public class MoveCommand : ICommand
    {
        private CharacterMovement _charMovement;
        private Vector3 _moveDirection;
        public MoveCommand(CharacterMovement character, Vector3 direction)
        {
            _charMovement = character;
            _moveDirection = direction;
        }

        public void Execute()
        {
            _charMovement.MoveToDirection(_moveDirection);
        }

        public void Undo()
        {
            _charMovement.MoveToDirection(_moveDirection * -1);
        }
    }
}