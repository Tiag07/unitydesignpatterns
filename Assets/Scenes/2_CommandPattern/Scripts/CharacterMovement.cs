﻿using UnityEngine;

namespace CommandPattern
{
    public class CharacterMovement : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 2f;
        public bool InMovingProcess { get; private set; } = false;
        private Vector3 destination;

        private void Update()
        {
            if (InMovingProcess)
                MovingProcess();
        }

        public void MoveToDirection(Vector3 moveDirection)
        {
            if (InMovingProcess) return;

            destination = transform.position + moveDirection;
            InMovingProcess = true;
        }

        private void MovingProcess()
        {
            float currentDistanceFromDestination = Vector3.Distance(transform.position, destination);
            if (Mathf.Approximately(currentDistanceFromDestination, 0) == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
            }
            else InMovingProcess = false;
        }
    }
}