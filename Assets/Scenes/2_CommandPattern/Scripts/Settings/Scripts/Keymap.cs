﻿using UnityEngine;

[CreateAssetMenu(fileName = "Keymap", menuName = "ScriptableObjects/CommandPattern/Keymap")]
public class Keymap : ScriptableObject
{
    [HideInInspector] public bool InChangingProcess = false;
    public KeyCode KeyUp = KeyCode.W;
    public KeyCode KeyDown = KeyCode.S;
    public KeyCode KeyLeft = KeyCode.A;
    public KeyCode KeyRight = KeyCode.D;
    public KeyCode KeyUndo = KeyCode.Backspace;
}
