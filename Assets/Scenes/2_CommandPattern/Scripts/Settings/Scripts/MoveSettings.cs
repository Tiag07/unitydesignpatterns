﻿using UnityEngine;

[CreateAssetMenu(fileName = "MoveSettings", menuName = "ScriptableObjects/CommandPattern/MoveSettings")]

public class MoveSettings : ScriptableObject
{
    public Vector3 upMovement;
    public Vector3 downMovement;
    public Vector3 leftMovement;
    public Vector3 rightMovement;
}
