﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CommandPattern
{
    public class UI_PlayerKeysConfig : MonoBehaviour
    {
        [SerializeField] private Keymap keymap;
        [SerializeField] private Text txtKeyUpValue;
        [SerializeField] private Text txtKeyDownValue;
        [SerializeField] private Text txtKeyLeftValue;
        [SerializeField] private Text txtKeyRightValue;
        [SerializeField] private Text txtKeyUndoValue;
        private bool waitingForNewButton = false;
        enum KeyForChanging { Up, Down, Left, Right, Undo }
        KeyForChanging keyForChanging;

        private void Start()
        {
            waitingForNewButton = false;
            RefreshKeyTexts();
        }
        void RefreshKeyTexts()
        {
            txtKeyUpValue.text = keymap.KeyUp.ToString();
            txtKeyDownValue.text = keymap.KeyDown.ToString();
            txtKeyLeftValue.text = keymap.KeyLeft.ToString();
            txtKeyRightValue.text = keymap.KeyRight.ToString();
            txtKeyUndoValue.text = keymap.KeyUndo.ToString();
        }
        public void Button_SetKeyUp()
        {
            EnterInChangingProcess(KeyForChanging.Up, txtKeyUpValue);
        }
        public void Button_SetKeyDown()
        {
            EnterInChangingProcess(KeyForChanging.Down, txtKeyDownValue);
        }
        public void Button_SetKeyLeft()
        {
            EnterInChangingProcess(KeyForChanging.Left, txtKeyLeftValue);
        }
        public void Button_SetKeyRight()
        {
            EnterInChangingProcess(KeyForChanging.Right, txtKeyRightValue);
        }
        public void Button_SetKeyUndo()
        {
            EnterInChangingProcess(KeyForChanging.Undo, txtKeyUndoValue);
        }

        void EnterInChangingProcess(KeyForChanging currentKey, Text txtOfThisKey)
        {
            keyForChanging = currentKey;          
            txtOfThisKey.text = "-";
            waitingForNewButton = true;
            keymap.InChangingProcess = true;
        }

        private void OnGUI()
        {
            if (waitingForNewButton)
            {
                if(Event.current.isKey && Event.current.type == EventType.KeyDown)
                {
                    SetNewValueToKey(Event.current.keyCode);
                    waitingForNewButton = false;
                    StartCoroutine(EnableKeyMapInteractionCoroutine());
                }
            }
        }
        IEnumerator EnableKeyMapInteractionCoroutine()
        {
            yield return new WaitForSeconds(0.1f);
            keymap.InChangingProcess = false;
        }

        private void SetNewValueToKey(KeyCode newKeyCode)
        {
            switch (keyForChanging)
            {
                case KeyForChanging.Up:
                    keymap.KeyUp = newKeyCode;
                    break;

                case KeyForChanging.Down:
                    keymap.KeyDown = newKeyCode;
                    break;

                case KeyForChanging.Left:
                    keymap.KeyLeft = newKeyCode;
                    break;

                case KeyForChanging.Right:
                    keymap.KeyRight = newKeyCode;
                    break;

                case KeyForChanging.Undo:
                    keymap.KeyUndo = newKeyCode;
                    break;
            }
            RefreshKeyTexts();
        }
    }
}