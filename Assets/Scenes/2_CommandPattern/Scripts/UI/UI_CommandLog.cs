﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CommandPattern
{
    public class UI_CommandLog : MonoBehaviour
    {
        [SerializeField] CharacterInputHandler inputHandler;
        [SerializeField] Text txtCommandLog;
        Stack<string> _commandNameList = new Stack<string>();

        private void Start()
        {
            inputHandler.commandActivated += AddCommandName;
            inputHandler.undoCommandActivated += RemoveLastCommandName;
            RefreshLog();
        }

        private void AddCommandName(string commandName)
        {
            _commandNameList.Push(commandName);
            RefreshLog();
        }
        private void RemoveLastCommandName()
        {
            if (_commandNameList.Count == 0) return;
            _commandNameList.Pop();
            RefreshLog();
        }
        private void RefreshLog()
        {
            txtCommandLog.text = "";
            foreach (var command in _commandNameList)
            {
                string newLine = string.Concat(command, "\n");
                txtCommandLog.text += newLine;
            }
        }
    }
}