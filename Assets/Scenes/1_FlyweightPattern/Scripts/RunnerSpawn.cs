﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlyweightPattern
{
    public class RunnerSpawn : MonoBehaviour
    {
        public enum Mode { NoFlyweight, Flyweight }
        [SerializeField] Mode mode;
        [SerializeField] private float spawnIntervalInSeconds = 1.5f;
        [SerializeField] GameObject noFlyweightRunnerPrefab;
        [SerializeField] GameObject flyweightRunnerPrefab;
        [SerializeField] Transform spawnPoint;
        void Start()
        {
            StartCoroutine(SpawnCoroutine());
        }

        IEnumerator SpawnCoroutine()
        {
            yield return new WaitForSeconds(spawnIntervalInSeconds);

            GameObject newRunner;
            if (mode == Mode.NoFlyweight)
            {
                newRunner = Instantiate(noFlyweightRunnerPrefab, spawnPoint);
            }
            else
                newRunner = Instantiate(flyweightRunnerPrefab, spawnPoint);

            RandomizeRunnerRotation(newRunner);
            StartCoroutine(SpawnCoroutine());           
        }

        void RandomizeRunnerRotation(GameObject runner)
        {
            float yFactor = Random.Range(0, 360);
            runner.transform.rotation = Quaternion.Euler(0, yFactor, 0);
        }
    }
}