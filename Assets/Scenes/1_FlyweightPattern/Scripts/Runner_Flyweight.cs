﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlyweightPattern
{
    public class Runner_Flyweight : MonoBehaviour
    {
        [SerializeField]
        RunnerStats runnerStats;

        [SerializeField]
        private float _currentLife;

        [SerializeField]
        private MeshRenderer[] meshRenderers;

        private void Start()
        {
            _currentLife = runnerStats.MaxLife;
            ToColorGraphics();
        }
        void ToColorGraphics()
        {
            foreach (var rend in meshRenderers)
            {
                MaterialPropertyBlock newPropertyBlock = new MaterialPropertyBlock();
                newPropertyBlock.SetColor("_Color", runnerStats.MeshColor);
                rend.SetPropertyBlock(newPropertyBlock);
            }
        }
        void Update()
        {
            RunForward();
            Rotate();
            LoseLife();
        }

        void RunForward()
        {
            transform.position += transform.forward * runnerStats.Speed * Time.deltaTime;
        }

        void Rotate()
        {
            transform.Rotate(Vector3.up * runnerStats.RotateSpeed * Time.deltaTime);
        }

        void LoseLife()
        {
            if (_currentLife <= 0)
            {
                _currentLife = 0;
                return;
            }
            _currentLife -= Time.deltaTime;
        }
    }
}