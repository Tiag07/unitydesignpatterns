﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlyweightPattern
{
    public class Runner_NoFlyweight : MonoBehaviour
    {
        [SerializeField]
        private float maxLife = 100f;
        [SerializeField]
        private float _currentLife;
        [SerializeField]
        private float speed = 2f;
        [SerializeField]
        private float rotateSpeed = 10f;
        [SerializeField]
        private Color meshColor = Color.white;
        [SerializeField]
        private MeshRenderer[] meshRenderers;

        private void Start()
        {
            _currentLife = maxLife;
            ToColorGraphics();
        }
        void ToColorGraphics()
        {
            foreach (var rend in meshRenderers)
            {
                rend.material.SetColor("_Color", meshColor);
            }
        }
        void Update()
        {
            RunForward();
            Rotate();
            LoseLife();
        }

        void RunForward()
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        void Rotate()
        {
            transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
        }

        void LoseLife()
        {
            if (_currentLife <= 0)
            {
                _currentLife = 0;
                return;
            }
            _currentLife -= Time.deltaTime;
        }
    }
}