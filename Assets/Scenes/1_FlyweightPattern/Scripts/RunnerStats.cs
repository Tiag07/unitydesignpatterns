﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlyweightPattern
{
    [CreateAssetMenu(fileName = "RunnerStats", menuName = "ScriptableObjects/NewRunnerStats")]
    public class RunnerStats : ScriptableObject
    {
        [SerializeField] private float maxLife = 100f;
        [SerializeField] private float speed = 2f;
        [SerializeField] private float rotateSpeed = 10f;
        [SerializeField] private Color meshColor = Color.white;

        public float MaxLife { get => maxLife; }
        public float Speed { get => speed; }
        public float RotateSpeed { get => rotateSpeed; }
        public Color MeshColor { get => meshColor; }
    }
}